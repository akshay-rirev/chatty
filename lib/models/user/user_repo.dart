import 'package:chatty/models/user/user.dart';
import 'package:chatty/services/database.dart';
import 'package:meta/meta.dart';

class UserRepo {
  static final UserRepo instance = UserRepo();

  Future<User> getUser({
    @required final String userId,
  }) async {
    return User.fromJson(
      (await Database.instance.query(
        doc: '''
          query getUser(
            \$user_id: uuid!,
          ) {
            users_by_pk(id: \$user_id) {
              name
            }
          }
        ''',
        variables: {
          'user_id': userId,
        },
      ))['users_by_pk'],
    );
  }

  Future<User> createUser({
    @required final User user,
  }) async {
    return User.fromJson(
      (await Database.instance.mutation(
        doc: '''
          mutation createUser(\$name: String!) {
            insert_users_one(object: {name: \$name}) {
              id
              name
            }
          }
        ''',
        variables: {
          'name': user.name,
        },
      ))['insert_users_one'],
    );
  }

  Future<User> updateUser({
    @required final User user,
  }) async {
    return User.fromJson(
      (await Database.instance.mutation(
        doc: '''
          mutation updateUserName(
            \$update_user: users_set_input!,
          ) {
            update_users(
              where: {},
              _set: \$update_user,
            ) {
              returning {
                name
              }
            }
          }
        ''',
        variables: {
          'update_user': user.toUpdateJson,
        },
      ))['update_users']['returning'][0],
    );
  }
}
