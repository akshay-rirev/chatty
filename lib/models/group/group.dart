import 'package:chatty/services/null_checker.dart';
import 'package:flutter/material.dart';
import 'package:chatty/extensions/string_extension.dart';

class Group {
  final String id;
  final String name;
  final String preview;
  final DateTime createdAt;
  final DateTime updatedAt;

  Group({
    this.id,
    @required this.name,
    this.preview,
    this.createdAt,
    this.updatedAt,
  });

  factory Group.fromJson(final Map<String, dynamic> json) {
    if (blank(json)) {
      return null;
    } else {
      return Group(
        name: json['name'],
        preview: json['preview'],
        createdAt: (json['created_at'] as String).toDateTime(),
        updatedAt: (json['updated_at'] as String).toDateTime(),
      );
    }
  }

  static List<Group> listFromJson(final List<dynamic> jsons) {
    if (blank(jsons)) {
      return [];
    } else {
      return jsons.map<Group>((final dynamic json) {
        return Group.fromJson(json);
      }).toList();
    }
  }

  Map<String, dynamic> get toUpdateJson {
    return {
      'name': name,
    };
  }
}
