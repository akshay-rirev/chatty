import 'package:chatty/models/group/group.dart';
import 'package:chatty/services/database.dart';
import 'package:chatty/services/storage.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

class GroupRepo {
  static final GroupRepo instance = GroupRepo();

  Future<List<Group>> getGroups({
    @required final DateTime since,
  }) async {
    return Group.listFromJson(
      (await Database.instance.query(
        doc: '''
          query getGroupsByMini(\$since: timestamptz!) {
            groups(
              where: {
                updated_at: {_gte: \$since},
              }, 
              order_by: {
                updated_at: desc,
              },
            ) {
              id
              name
              created_at
              updated_at
            }
          }
        ''',
        variables: {
          'since': since.toUtc().toString(),
        },
      ))['groups'],
    );
  }

  Future<Group> createGroup(String name) async {
    return Group.fromJson(
      (await Database.instance.mutation(
        doc: ''' 
        mutation createGroupMutation(\$name: String) {
          insert_groups_one(object: {name: \$name}) {
            id
            name
            updated_at
            created_at
          }
        }
        ''',
        variables: {
          'name': name,
        },
      ))['insert_groups_one'],
    );
  }

  Stream<DateTime> getPing() {
    return Database.instance.subscription(
      doc: '''
        subscription getPing {
          groups(
            order_by: {
              updated_at: desc,
            },
            limit: 1,
          ) {
            updated_at
          }
        }
      ''',
      variables: {},
      key: Storage.instance.userId,
    ).map<DateTime>((Map<String, dynamic> convert) {
      return Group.listFromJson(convert['groups']).fold<DateTime>(
        DateTime(2020, 1, 1),
        (final DateTime previousValue, final Group group) {
          if (previousValue.isBefore(group.updatedAt)) {
            return group.updatedAt;
          } else {
            return previousValue;
          }
        },
      );
    });
  }
}
