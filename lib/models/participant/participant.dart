import 'package:chatty/models/user/user.dart';
import 'package:chatty/services/null_checker.dart';
import 'package:flutter/material.dart';

class Participant {
  final User user;

  Participant({
    @required this.user,
  });

  factory Participant.fromJson(final Map<String, dynamic> json) {
    if (blank(json)) {
      return null;
    } else {
      return Participant(
        user: User.fromJson(json['user']),
      );
    }
  }

  Map<String, dynamic> get toUpdateJson {
    return {
      'user_id': user.id,
    };
  }
}
