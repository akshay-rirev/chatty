import 'package:chatty/models/group/group.dart';
import 'package:chatty/models/user/user.dart';
import 'package:chatty/services/null_checker.dart';
import 'package:chatty/extensions/string_extension.dart';

class Message {
  final String id;
  final User user;
  final Group group;
  final String body;
  final String scrubbedBody;
  final String userName;
  final DateTime createdAt;
  final DateTime updatedAt;
  final bool deleted;
  bool selected;

  Message({
    this.id,
    this.user,
    this.group,
    this.body,
    this.scrubbedBody,
    this.userName,
    this.createdAt,
    this.updatedAt,
    this.deleted,
    this.selected = false,
  });

  factory Message.fromJson(final Map<String, dynamic> json) {
    if (blank(json)) {
      return null;
    } else {
      return Message(
        id: json['id'],
        user: User.fromJson(json['user']),
        group: Group.fromJson(json['group']),
        body: json['body'],
        scrubbedBody: json['scrubbed_body'],
        userName: json['user_name'],
        createdAt: (json['created_at'] as String).toDateTime(),
        updatedAt: (json['updated_at'] as String).toDateTime(),
        deleted: json['deleted'],
      );
    }
  }

  static List<Message> listFromJson(final List<dynamic> jsons) {
    if (blank(jsons)) {
      return [];
    } else {
      return jsons.map<Message>((final dynamic json) {
        return Message.fromJson(json);
      }).toList();
    }
  }
}
