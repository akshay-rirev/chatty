import 'package:meta/meta.dart';

abstract class GroupEvent {}

class LoadGroup extends GroupEvent {}

class CreateGroup extends GroupEvent {
  final String name;

  CreateGroup(this.name);
}

class LoadGroups extends GroupEvent {
  final DateTime since;

  LoadGroups({
    @required final this.since,
  });
}
