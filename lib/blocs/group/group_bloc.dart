import 'dart:async';

import 'package:chatty/blocs/group/group_event.dart';
import 'package:chatty/blocs/group/group_state.dart';
import 'package:chatty/models/group/group.dart';
import 'package:chatty/models/group/group_repo.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class GroupBloc extends Bloc<GroupEvent, GroupState> {
  GroupBloc() : super(GroupLoading());

  final List<Group> groups = [];
  StreamSubscription<DateTime> pinger;

  @override
  Stream<GroupState> mapEventToState(
    final GroupEvent event,
  ) async* {
    if (event is LoadGroup) {
      yield GroupLoading();

      add(LoadGroups(since: DateTime(2020, 1, 1)));
    } else if (event is LoadGroups) {
      final List<Group> group = await GroupRepo.instance.getGroups(
        since: event.since,
      );

      group.forEach((final Group group) {
        if (this.groups.contains(group)) {
          this.groups[this.groups.indexOf(group)] = group;
        } else {
          this.groups.insert(0, group);
        }
      });

      pinger ??= GroupRepo.instance.getPing().listen((
        final DateTime remoteMaxima,
      ) {
        final DateTime localMaxima = group.fold<DateTime>(
          DateTime(2020, 1, 1),
          (final DateTime previousValue, final Group group) {
            if (previousValue.isBefore(group.updatedAt)) {
              return group.updatedAt;
            } else {
              return previousValue;
            }
          },
        );

        if (localMaxima.isBefore(remoteMaxima)) {
          add(LoadGroups(since: localMaxima));
        }
      });

      yield GroupLoaded();
    } else if (event is CreateGroup) {
      yield GroupLoading();

      await GroupRepo.instance.createGroup(event.name);

      yield GroupLoaded();
    }
  }

  @override
  Future<void> close() async {
    await pinger.cancel();
    return super.close();
  }
}
