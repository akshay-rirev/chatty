import 'package:meta/meta.dart';

abstract class SignUpEvent {}

class LoadSignUp extends SignUpEvent {}

class CreateUser extends SignUpEvent {
  final String name;

  CreateUser({
    @required final this.name,
  });
}
