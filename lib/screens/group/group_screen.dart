import 'package:chatty/blocs/group/group_bloc.dart';
import 'package:chatty/blocs/group/group_event.dart';
import 'package:chatty/blocs/group/group_state.dart';
import 'package:chatty/services/null_checker.dart';
import 'package:chatty/services/wayfinder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class GroupScreen extends StatefulWidget {
  @override
  _GroupScreenState createState() => _GroupScreenState();
}

class _GroupScreenState extends State<GroupScreen> {
  //ignore: close_sinks
  GroupBloc groupBloc;
  TextEditingController nameController = TextEditingController();

  @override
  void initState() {
    groupBloc = GroupBloc();
    groupBloc.add(LoadGroup());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<GroupBloc>(
      create: (final BuildContext context) {
        return groupBloc;
      },
      child: BlocBuilder<GroupBloc, GroupState>(
        builder: (
          final BuildContext context,
          final GroupState state,
        ) {
          return Scaffold(
            appBar: AppBar(),
            body: body(state),
            floatingActionButton: FloatingActionButton(
              child: Icon(Icons.add),
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                    title: Text("Enter Group Name"),
                    content: ListTile(
                      title: TextFormField(
                        controller: nameController,
                      ),
                    ),
                    actions: [
                      RaisedButton(
                        child: Text("Create"),
                        onPressed: () {
                          groupBloc.add(CreateGroup(nameController.text));
                          Wayfinder.instance.pop();
                          nameController.clear();
                        },
                      )
                    ],
                  ),
                );
              },
            ),
          );
        },
      ),
    );
  }

  Widget body(GroupState state) {
    if (state is GroupLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else {
      return RefreshIndicator(
        child: ListView.builder(
          itemBuilder: (final BuildContext context, int index) {
            return ListTile(
              title: Text(
                groupBloc.groups[index].name,
              ),
              subtitle: blank(groupBloc.groups[index].preview)
                  ? null
                  : Text(groupBloc.groups[index].preview),
              onTap: () {
                // Wayfinder.instance.showChannel(
                //   channel: groupBloc.groups[index],
                // );
              },
            );
          },
          itemCount: groupBloc.groups.length,
        ),
        onRefresh: () async {
          BlocProvider.of<GroupBloc>(context).add(LoadGroup());
        },
      );
    }
  }
}
