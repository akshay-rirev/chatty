import 'package:chatty/blocs/sign_up/sign_up_bloc.dart';
import 'package:chatty/blocs/sign_up/sign_up_event.dart';
import 'package:chatty/blocs/sign_up/sign_up_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  //ignore: close_sinks
  SignUpBloc signUpBloc;
  TextEditingController nameController = TextEditingController();

  @override
  void initState() {
    signUpBloc = SignUpBloc();
    signUpBloc.add(LoadSignUp());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<SignUpBloc>(
      create: (final BuildContext context) {
        return signUpBloc;
      },
      child: BlocBuilder<SignUpBloc, SignUpState>(
        builder: (
          final BuildContext context,
          final SignUpState state,
        ) {
          return Scaffold(
            appBar: AppBar(),
            body: body(state),
          );
        },
      ),
    );
  }

  Widget body(SignUpState state) {
    if (state is SignUpLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else {
      return Padding(
        padding: EdgeInsets.all(24),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            TextFormField(
              decoration: InputDecoration(
                  hintText: 'Enter your name', labelText: 'Name'),
              controller: nameController,
            ),
            RaisedButton(
              child: Text("Create User"),
              onPressed: () {
                signUpBloc.add(CreateUser(name: nameController.text));
              },
            ),
          ],
        ),
      );
    }
  }
}
