import 'package:chatty/screens/group/group_screen.dart';
import 'package:chatty/screens/sign_up/sign_up_screen.dart';
import 'package:chatty/services/storage.dart';
import 'package:chatty/services/wayfinder.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Future _loadPreferencesFuture;
  final GlobalKey<NavigatorState> _navigatorKey = GlobalKey<NavigatorState>();

  @override
  void initState() {
    _loadPreferencesFuture = Storage.instance.load();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _loadPreferencesFuture,
      builder: (
        final BuildContext context,
        final AsyncSnapshot<dynamic> snapshot,
      ) {
        if (snapshot.connectionState == ConnectionState.waiting ||
            snapshot.connectionState == ConnectionState.active) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else {
          return GestureDetector(
            onTap: () {
              WidgetsBinding.instance.focusManager.primaryFocus?.unfocus();
            },
            child: MaterialApp(
              navigatorKey: _navigatorKey,
              title: 'Chatty',
              debugShowCheckedModeBanner: false,
              home: home(),
            ),
          );
        }
      },
    );
  }

  Widget home() {
    Wayfinder.instance.navigatorKey = _navigatorKey;

    if (Storage.instance.userId == null) {
      return SignUpScreen();
    } else {
      return GroupScreen();
    }
  }
}
