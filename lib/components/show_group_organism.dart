import 'package:chatty/blocs/organisms/show_group_bloc.dart';
import 'package:chatty/blocs/organisms/show_group_event.dart';
import 'package:chatty/blocs/organisms/show_group_state.dart';
import 'package:chatty/models/group/group.dart';
import 'package:chatty/models/message/message.dart';
import 'package:chatty/services/null_checker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ShowGroupOrganism extends StatefulWidget {
  final Group group;

  const ShowGroupOrganism({
    @required final this.group,
  });

  @override
  _ShowChannelOrganismState createState() {
    return _ShowChannelOrganismState();
  }
}

class _ShowChannelOrganismState extends State<ShowGroupOrganism> {
  final TextEditingController _bodyController = TextEditingController();

  ShowGroupBloc _showChannelBloc;

  @override
  void initState() {
    _bodyController.addListener(() {
      setState(() {});
    });

    _showChannelBloc = ShowGroupBloc(
      group: widget.group,
    );

    _showChannelBloc.add(LoadShowGroup());

    super.initState();
  }

  @override
  void dispose() {
    _bodyController.dispose();

    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    return BlocProvider<ShowGroupBloc>(
      create: (final BuildContext context) {
        return _showChannelBloc;
      },
      child: BlocBuilder<ShowGroupBloc, ShowGroupState>(
        builder: (
          final BuildContext context,
          final ShowGroupState state,
        ) {
          return Scaffold(
            appBar: _appBar(state),
            body: _body(state),
          );
        },
      ),
    );
  }

  AppBar _appBar(final ShowGroupState state) {
    if (state is ShowGroupLoaded) {
      return AppBar(
        actions: [
          if (_showChannelBloc.selectedMessages.length > 0)
            Row(
              children: [
                if (_showChannelBloc.copyableMessages.length > 0)
                  IconButton(
                    icon: Icon(Icons.content_copy),
                    onPressed: () {
                      _showChannelBloc.add(CopyMessages());
                    },
                  ),
                if (_showChannelBloc.archivableMessages.length > 0)
                  IconButton(
                    icon: Icon(Icons.delete_forever),
                    onPressed: () {
                      _showChannelBloc.add(
                        ArchiveMessages(),
                      );
                    },
                  ),
              ],
            ),
        ],
      );
    } else {
      return AppBar(
        title: Text('Loading'),
      );
    }
  }

  Widget _body(final ShowGroupState state) {
    if (state is ShowGroupLoaded) {
      return WillPopScope(
        child: Column(
          children: <Widget>[
            Expanded(
              child: ListView.builder(
                reverse: true,
                itemBuilder: (final BuildContext context, final int index) {
                  return ListTile(
                    key: ValueKey(_showChannelBloc.messages[index].updatedAt),
                    title: Text(_showChannelBloc.messages[index].userName),
                    subtitle: Text(
                      _showChannelBloc.messages[index].scrubbedBody,
                    ),
                    onTap: () {
                      if (_showChannelBloc.selectedMessages.length > 0) {
                        if (_showChannelBloc.messages[index].selected) {
                          _showChannelBloc.add(
                            DeselectMessage(
                              message: _showChannelBloc.messages[index],
                            ),
                          );
                        } else {
                          _showChannelBloc.add(
                            SelectMessage(
                              message: _showChannelBloc.messages[index],
                            ),
                          );
                        }
                      }
                    },
                    onLongPress: () {
                      _showChannelBloc.add(
                        SelectMessage(
                          message: _showChannelBloc.messages[index],
                        ),
                      );
                    },
                    selected: _showChannelBloc.messages[index].selected,
                  );
                },
                itemCount: _showChannelBloc.messages.length,
              ),
            ),
            ListTile(
              title: TextField(
                controller: _bodyController,
                decoration: InputDecoration(
                  hintText: 'Type a message',
                ),
                keyboardType: TextInputType.multiline,
                inputFormatters: [
                  LengthLimitingTextInputFormatter(1024),
                ],
              ),
              trailing: IconButton(
                icon: Icon(Icons.send),
                onPressed: blank(_bodyController.text)
                    ? null
                    : () {
                        _showChannelBloc.add(
                          CreateMessage(
                            message: Message(
                              body: _bodyController.text.trim(),
                            ),
                          ),
                        );
                        _bodyController.clear();
                      },
              ),
            ),
          ],
        ),
        onWillPop: () async {
          if (_showChannelBloc.selectedMessages.length > 0) {
            _showChannelBloc.add(
              DeselectMessages(),
            );

            return false;
          } else {
            return true;
          }
        },
      );
    } else {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
  }
}
